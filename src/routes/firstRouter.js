import { Router } from "express";
export let firstRouter = Router();

// 			url=localhost:8000,post at response "home get"
// 			url=localhost:8000,get at response "home get"
// 			url=localhost:8000,patch at response "home patch"
// 			url=localhost:8000,delete at response "home delete"

//         url=localhost:8000/name,post at response "name post"
// 		    url=localhost:8000/name,get at response "name get"
// 			url=localhost:8000/name,patch at response "name patch"
// 			url=localhost:8000/name,delete at response "name delete"

// url=localhost:8000/admin,post at response "admin post"
// url=localhost:8000/admin,get at response "admin get"
// url=localhost:8000/admin,patch at response "admin patch"
// url=localhost:8000/admin,delete at response "admin delete"

firstRouter
  .route("/") //localhost:8000
  .post((req, res) => {
    res.json("home post");
  })
  .get((req, res) => {
    res.json("home get");
  })
  .patch((req, res) => {
    res.json("home patch");
  })
  .delete((req, res) => {
    res.json("home delete");
  });

firstRouter
  .route("/name") //localhost:8000/name
  .post((req, res) => {
    res.json("name post");
  })
  .get((req, res) => {
    res.json("name get");
  })
  .patch((req, res) => {
    res.json("name patch");
  })
  .delete((req, res) => {
    res.json("name delete");
  });

firstRouter
  .route("/admin") //localhost:8000/admin
  .post((req, res) => {
    res.json("admin post");
  })
  .get((req, res) => {
    res.json("admin get");
  })
  .patch((req, res) => {
    res.json("admin patch");
  })
  .delete((req, res) => {
    res.json("admin delete");
  });
