import { Router } from "express";

export let foodRouter = Router();

foodRouter
  .route("/")
  .post((req, res) => {
    console.log(req.body);
    res.json({
      success: true,
      message: "food created successfully",
    });
  })
  .get((req, res) => {
    res.json({
      success: true,
      message: "food read successfully",
    });
  })
  .patch((req, res) => {
    res.json({
      success: true,
      message: "food patched successfully",
    });
  })
  .delete((req, res) => {
    res.json({
      success: true,
      message: "food deleted successfully",
    });
  });
