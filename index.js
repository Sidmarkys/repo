import express, { json } from "express";
import { port } from "./src/constant.js";
import { firstRouter } from "./src/routes/firstRouter.js";
import { foodRouter } from "./src/routes/foodRouter.js";
//make express app

let expressApp = express();
expressApp.use(json());
//attached port to that expressApp
expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`);
});

expressApp.use("/", firstRouter);
expressApp.use("/food", foodRouter);
